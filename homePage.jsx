import React,{Component} from "react";
class HomePage extends Component{
    state={
        name:this.props.name,
    }
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state}
        s1.name=input.value
        this.setState(s1);
    }
handleSubmit=(e)=>{
    e.preventDefault();
    this.props.onSubmit(this.state.name)
}
    render(){
        let {name}=this.state;
        return (
            <div className="container">
                <h3 className="text-center" >Welcome to the Quiz Show</h3>
                 <div className="form-group">
                <label>Name</label>
                <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={name}
                placeholder="Enter Your Name"
                onChange={this.handleChange}
                onBlurr={this.handleValidate}
                />
                </div>
                <br/>
                <div className="text-center">
                <button className="btn btn-primary "onClick={this.handleSubmit}>Start Quiz</button>
            </div>
            </div>
        )
    }
}
export default HomePage;