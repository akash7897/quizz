import React,{Component} from "react";
import HomePage from "./homePage"
class MainComponents extends Component{
    state={
        quizData:[
            {image:"https://images.pexels.com/photos/145939/pexels-photo-145939.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Tiger","Lion","Cheetah"],answer:1},
            {image:"https://images.pexels.com/photos/45201/kitty-cat-kitten-pet-45201.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Lion","Cat","Mouse"],answer:2},
            {image:"https://images.pexels.com/photos/34231/antler-antler-carrier-fallow-deer-hirsch.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["squirell","Deer","Beer"],answer:2},
            {image:"https://images.pexels.com/photos/750539/pexels-photo-750539.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Zebra","Cow","Cat"],answer:1},
            {image:"https://images.pexels.com/photos/45911/peacock-plumage-bird-peafowl-45911.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Lion","Crow","Peacock"],answer:3},
            {image:"https://images.pexels.com/photos/1829979/pexels-photo-1829979.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Cow","Monkey","Dog"],answer:2},
            {image:"https://images.pexels.com/photos/76957/tree-frog-frog-red-eyed-amphibian-76957.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Snake","Lizard","Cockroach"],answer:2},
            {image:"https://images.pexels.com/photos/9946097/pexels-photo-9946097.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Zebra","Mouse","Goat"],answer:3},
            {image:"https://images.pexels.com/photos/634613/pexels-photo-634613.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Hose","Tiger","Buffalow"],answer:1},
            {image:"https://images.pexels.com/photos/1618606/pexels-photo-1618606.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Whale","Fish","Tortoise"],answer:3},
            {image:"https://images.pexels.com/photos/133394/pexels-photo-133394.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Buffalow","Elephant","Cow"],answer:2},
            {image:"https://images.pexels.com/photos/51312/kiwi-fruit-vitamins-healthy-eating-51312.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Kiwi","Guava","Orange"],answer:1},
            {image:"https://images.pexels.com/photos/102104/pexels-photo-102104.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Banana","Apple","Kiwi"],answer:2},
            {image:"https://images.pexels.com/photos/207085/pexels-photo-207085.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Apple","Mango","Orange"],answer:3},
        {image:"https://images.pexels.com/photos/1343537/pexels-photo-1343537.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Mango","Apple","Orange"],answer:1},
        {image:"https://images.pexels.com/photos/947879/pexels-photo-947879.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Papaya","Apple","Pineapple"],answer:3},
    {image:"https://images.pexels.com/photos/2872755/pexels-photo-2872755.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Banana","Papaya","Mango"],answer:1},
    {image:"https://images.pexels.com/photos/763197/pexels-photo-763197.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Crow","Peigon","Parrot"],answer:2},
    {image:"https://images.pexels.com/photos/52509/penguins-emperor-antarctic-life-52509.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Panda","Penguin","Whale"],answer:2},
    {image:"https://images.pexels.com/photos/7506265/pexels-photo-7506265.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",options:["Penguin","Elephant","Panda"],answer:3},

        ],
        view:0,
        questions:[],
        questionViewIndex:0,
        names:"",
        ans:0,
        WrongAnswerMessage:"",
        wrongAnswer:[],
    }
    handleName=(name)=>{
        let s1={...this.state};
        s1.names=name;
        s1.view=1;
        s1.questions=s1.quizData.sort(() => Math.random() - Math.random()).slice(0, 5)
        this.setState(s1);
    }
    nextQuestion=(index)=>{
        let s1={...this.state};
        console.log( s1.questionViewIndex)
        if (s1.questions[s1.questionViewIndex].answer===index+1){
            if(s1.questionViewIndex<4){
                s1.questionViewIndex=s1.questionViewIndex+1
                s1.wrongAnswerMessage=""
                
            }
               else{
                s1.view=2;  
               }
                
        }
        else{
            s1.wrongAnswerMessage="Wrong Answer"
            s1.wrongAnswer.push(s1.wrongAnswerMessage);
           
        }
        
        
        
        this.setState(s1);
    }
    restartQuiz=()=>{
        let s1={...this.state}
        s1.view=1;
        s1.questionViewIndex=0;
        s1.wrongAnswer=[];
        s1.questions=s1.quizData.sort(() => Math.random() - Math.random()).slice(0, 5)
        this.setState(s1);
    }
    render(){
        let {quizData,view,questionViewIndex,questions,wrongAnswerMessage,ans,names,wrongAnswer}=this.state
        let name=""
        console.log(quizData)
        console.log(questionViewIndex)
         console.log(wrongAnswerMessage)
        return view===0
        ?<React.Fragment>
               <HomePage 
               name={name}
               onSubmit={this.handleName}
               />
            
        </React.Fragment>
        :view===1
        ?(
            
                    <div className="container text-center">
                        <div className="row">
    <div className="col xs-6 md-4">
    <img 
      src={questions[questionViewIndex].image}
      alt="new"
      fluid="true"
      width="250" 
      height="250"
      />
      
    </div>
    </div>
    <br/>
    {questions[questionViewIndex].options.map((o,index)=>(
<button className="btn btn-primary m-2" onClick={()=>this.nextQuestion(index)}>{o}</button>
    ))
    }
    {wrongAnswerMessage?<h6 className="text-center text-danger">Wrong Answer</h6>:"" }
                        </div>
        
               
    
        )
        :view===2
        ?<div className="container">
            <h3 className="text-center text-success">Quiz Over</h3>

            <div className="text-center">
            <h3>Thanks {names} for playing the quiz</h3>
            <h3>Numbers of Errors : {wrongAnswer.length}</h3>
            <button className="btn btn-primary btn-lg m-2"onClick={()=>this.restartQuiz()}>Retry</button>
            </div>
            
        </div>
        :""
    }
}
export default MainComponents;